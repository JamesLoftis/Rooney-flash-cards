﻿using System.Collections.Generic;
using System.Linq;
using RooneyFoundationCardApp.Extensions;

namespace RooneyFoundationCardApp.Data
{
    public class Seed
    {
        public static void DoSeed(IDatabaseContext context)
        {
            ClearTables(context);
            SeedCardData(context);
            SaveSeedData(context);
            SeedWordData(context);
            SeedTraits(context);
            SaveSeedData(context);
        }

        private static void ClearTables(IDatabaseContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }

        private static void SaveSeedData(IDatabaseContext context) => context.SaveChanges(true);


        private static void SeedCardData(IDatabaseContext context)
        {
            context.Cards.Add(new Card("a", "TestCards/Basic/1.01"));
            context.Cards.Add(new Card("b", "TestCards/Basic/1.02"));
            context.Cards.Add(new Card("c", "TestCards/Basic/1.03"));
            context.Cards.Add(new Card("d", "TestCards/Basic/1.04"));
            context.Cards.Add(new Card("e", "TestCards/Basic/1.05"));
            context.Cards.Add(new Card("f", "TestCards/Basic/1.06"));
            context.Cards.Add(new Card("g", "TestCards/Basic/1.07"));
            context.Cards.Add(new Card("h", "TestCards/Basic/1.08"));
            context.Cards.Add(new Card("i", "TestCards/Basic/1.09"));
            context.Cards.Add(new Card("j", "TestCards/Basic/1.10"));
            context.Cards.Add(new Card("k", "TestCards/Basic/1.11"));
            context.Cards.Add(new Card("l", "TestCards/Basic/1.12"));
            context.Cards.Add(new Card("m", "TestCards/Basic/1.13"));
            context.Cards.Add(new Card("n", "TestCards/Basic/1.14"));
            context.Cards.Add(new Card("o", "TestCards/Basic/1.15"));
            context.Cards.Add(new Card("p", "TestCards/Basic/1.16"));
            context.Cards.Add(new Card("qu", "TestCards/Basic/1.17"));
            context.Cards.Add(new Card("r", "TestCards/Basic/1.18"));
            context.Cards.Add(new Card("s", "TestCards/Basic/1.19"));
            context.Cards.Add(new Card("t", "TestCards/Basic/1.20"));
            context.Cards.Add(new Card("u", "TestCards/Basic/1.21"));
            context.Cards.Add(new Card("v", "TestCards/Basic/1.22"));
            context.Cards.Add(new Card("w", "TestCards/Basic/1.23"));
            context.Cards.Add(new Card("x", "TestCards/Basic/1.24"));
            context.Cards.Add(new Card("y", "TestCards/Basic/1.25"));
            context.Cards.Add(new Card("z", "TestCards/Basic/1.26"));
            context.Cards.Add(new Card("ch", "TestCards/Basic/1.28"));
            context.Cards.Add(new Card("ph", "TestCards/Basic/1.29"));
            context.Cards.Add(new Card("sh", "TestCards/Basic/1.30"));
            context.Cards.Add(new Card("th", "TestCards/Basic/1.31"));
            context.Cards.Add(new Card("wh", "TestCards/Basic/1.32"));
            context.Cards.Add(new Card("ck", "TestCards/Basic/1.33"));
            context.Cards.Add(new Card("ar", "TestCards/Basic/1.47"));
            context.Cards.Add(new Card("er", "TestCards/Basic/1.48"));
            context.Cards.Add(new Card("ir", "TestCards/Basic/1.49"));
            context.Cards.Add(new Card("or", "TestCards/Basic/1.50"));
            context.Cards.Add(new Card("ur", "TestCards/Basic/1.51"));
            context.Cards.Add(new Card("a with graphic", "TestCards/Basic/1.52"));
            context.Cards.Add(new Card("e with graphic", "TestCards/Basic/1.53"));
            context.Cards.Add(new Card("i with graphic", "TestCards/Basic/1.54"));
            context.Cards.Add(new Card("o with graphic", "TestCards/Basic/1.55"));
            context.Cards.Add(new Card("u with graphic", "TestCards/Basic/1.56"));
            context.Cards.Add(new Card("r with graphic", "TestCards/Basic/1.57"));
           
        }

        private static void SeedWordData(IDatabaseContext context)
        {
            context.Words.Add(Word.NewWord("dog", "d", "o", "g", context));
            context.Words.Add(Word.NewWord("bog", "b", "o", "g", context));
            context.Words.Add(Word.NewWord("bed", "b", "e", "d", context));
            context.Words.Add(Word.NewWord("fed", "f", "e", "d", context));
            context.Words.Add(Word.NewWord("red", "r", "e", "d", context));
            context.Words.Add(Word.NewWord("ned", "n", "e", "d", context));
            context.Words.Add(Word.NewWord("bat", "b", "a", "t", context));
            context.Words.Add(Word.NewWord("fat", "f", "a", "t", context));
            context.Words.Add(Word.NewWord("fed", "f", "e", "d", context));
            context.Words.Add(Word.NewWord("ned", "n", "e", "d", context));
            context.Words.Add(Word.NewWord("bot", "b", "o", "t", context));
            context.Words.Add(Word.NewWord("cot", "c", "o", "t", context));
            context.Words.Add(Word.NewWord("cog", "c", "o", "g", context));
            context.Words.Add(Word.NewWord("log", "l", "o", "t", context));
//            Card 1.02   Card 1.01   Card 1.02   bab 0.766274929
//Card 1.02   Card 1.01   Card 1.03   bac 0.745481761
//Card 1.02   Card 1.01   Card 1.04   bad 0.425087337
//Card 1.02   Card 1.01   Card 1.07   bag 0.612143213
//Card 1.02   Card 1.01   Card 1.13   bam 0.533993595
//Card 1.02   Card 1.01   Card 1.14   ban 0.291874959
//Card 1.02   Card 1.01   Card 1.16   bap 0.671936646
//Card 1.02   Card 1.01   Card 1.20   bat 0.234947533
//Card 1.02   Card 1.01   Card 1.24   bax 0.688798068
//Card 1.02   Card 1.05   Card 1.02   beb 0.309864328
//Card 1.02   Card 1.05   Card 1.03   bec 0.075534794
//Card 1.02   Card 1.05   Card 1.04   bed 0.052149028
//Card 1.02   Card 1.05   Card 1.07   beg 0.403345917
//Card 1.02   Card 1.05   Card 1.13   bem 0.4503572
//Card 1.02   Card 1.05   Card 1.14   ben 0.43197905
//Card 1.02   Card 1.05   Card 1.16   bep 0.938892744
//Card 1.02   Card 1.05   Card 1.20   bet 0.414957514
//Card 1.02   Card 1.05   Card 1.24   bex 0.679825248
//Card 1.02   Card 1.09   Card 1.02   bib 0.933318514
//Card 1.02   Card 1.09   Card 1.03   bic 0.76851784
//Card 1.02   Card 1.09   Card 1.04   bid 0.284245034
//Card 1.02   Card 1.09   Card 1.07   big 0.35198269
//Card 1.02   Card 1.09   Card 1.13   bim 0.777209139
//Card 1.02   Card 1.09   Card 1.14   bin 0.51233028
//Card 1.02   Card 1.09   Card 1.16   bip 0.80822345
//Card 1.02   Card 1.09   Card 1.20   bit 0.504465158
//Card 1.02   Card 1.09   Card 1.24   bix 0.763639313
//Card 1.02   Card 1.15   Card 1.02   bob 0.188330023
//Card 1.02   Card 1.15   Card 1.03   boc 0.243939249
//Card 1.02   Card 1.15   Card 1.04   bod 0.387452498
//Card 1.02   Card 1.15   Card 1.07   bog 0.789909472
//Card 1.02   Card 1.15   Card 1.13   bom 0.618248711
//Card 1.02   Card 1.15   Card 1.14   bon 0.642899961
//Card 1.02   Card 1.15   Card 1.16   bop 0.243038232
//Card 1.02   Card 1.15   Card 1.20   bot 0.474113206
//Card 1.02   Card 1.15   Card 1.24   box 0.722342324
//Card 1.03   Card 1.01   Card 1.02   cab 0.257313777
//Card 1.03   Card 1.01   Card 1.03   cac 0.893979959
//Card 1.03   Card 1.01   Card 1.04   cad 0.22975169
//Card 1.03   Card 1.01   Card 1.07   cag 0.996758448
//Card 1.03   Card 1.01   Card 1.13   cam 0.051329482
//Card 1.03   Card 1.01   Card 1.14   can 0.799285875
//Card 1.03   Card 1.01   Card 1.16   cap 0.744295068
//Card 1.03   Card 1.01   Card 1.20   cat 0.507724841
//Card 1.03   Card 1.01   Card 1.24   cax 0.443368238
//Card 1.03   Card 1.15   Card 1.02   cob 0.961817223
//Card 1.03   Card 1.15   Card 1.04   cod 0.709803857
//Card 1.03   Card 1.15   Card 1.07   cog 0.390995324
//Card 1.03   Card 1.15   Card 1.13   com 0.566478668
//Card 1.03   Card 1.15   Card 1.14   con 0.999323441
//Card 1.03   Card 1.15   Card 1.16   cop 0.999686695
//Card 1.03   Card 1.15   Card 1.20   cot 0.502123847
//Card 1.04   Card 1.01   Card 1.02   dab 0.482559213
//Card 1.04   Card 1.01   Card 1.03   dac 0.4581727
//Card 1.04   Card 1.01   Card 1.04   dad 0.414978823
//Card 1.04   Card 1.01   Card 1.07   dag 0.982330892
//Card 1.04   Card 1.01   Card 1.14   dan 0.18353789
//Card 1.04   Card 1.01   Card 1.16   dap 0.467698074
//Card 1.04   Card 1.01   Card 1.20   dat 0.277099498
//Card 1.04   Card 1.01   Card 1.24   dax 0.616062084
//Card 1.04   Card 1.05   Card 1.02   deb 0.786349504
//Card 1.04   Card 1.05   Card 1.03   dec 0.6711904
//Card 1.04   Card 1.05   Card 1.04   ded 0.984607314
//Card 1.04   Card 1.05   Card 1.07   deg 0.851018835
//Card 1.04   Card 1.05   Card 1.13   dem 0.191025334
//Card 1.04   Card 1.05   Card 1.14   den 0.188606336
//Card 1.04   Card 1.05   Card 1.16   dep 0.587922986
//Card 1.04   Card 1.05   Card 1.20   det 0.717201942
//Card 1.04   Card 1.05   Card 1.24   dex 0.695268679
//Card 1.04   Card 1.09   Card 1.02   dib 0.356597979
//Card 1.04   Card 1.09   Card 1.03   dic 0.442039793
//Card 1.04   Card 1.09   Card 1.04   did 0.931215952
//Card 1.04   Card 1.09   Card 1.07   dig 0.001027079
//Card 1.04   Card 1.09   Card 1.13   dim 0.839652166
//Card 1.04   Card 1.09   Card 1.14   din 0.488421054
//Card 1.04   Card 1.09   Card 1.16   dip 0.660135351
//Card 1.04   Card 1.09   Card 1.20   dit 0.79408262
//Card 1.04   Card 1.15   Card 1.02   dob 0.695690051
//Card 1.04   Card 1.15   Card 1.03   doc 0.689220044
//Card 1.04   Card 1.15   Card 1.04   dod 0.219266554
//Card 1.04   Card 1.15   Card 1.07   dog 0.946054315
//Card 1.04   Card 1.15   Card 1.13   dom 0.909060243
//Card 1.04   Card 1.15   Card 1.14   don 0.88566929
//Card 1.04   Card 1.15   Card 1.16   dop 0.606667626
//Card 1.04   Card 1.15   Card 1.20   dot 0.912598065
//Card 1.04   Card 1.15   Card 1.24   dox 0.334140708
//Card 1.06   Card 1.01   Card 1.02   fab 0.961806219
//Card 1.06   Card 1.01   Card 1.03   fac 0.376340883
//Card 1.06   Card 1.01   Card 1.04   fad 0.024754963
//Card 1.06   Card 1.01   Card 1.13   fam 0.794239402
//Card 1.06   Card 1.01   Card 1.14   fan 0.441883613
//Card 1.06   Card 1.01   Card 1.16   fap 0.994229673
//Card 1.06   Card 1.01   Card 1.20   fat 0.216660967
//Card 1.06   Card 1.01   Card 1.24   fax 0.610304463
//Card 1.06   Card 1.05   Card 1.02   feb 0.453003929
//Card 1.06   Card 1.05   Card 1.03   fec 0.087741001
//Card 1.06   Card 1.05   Card 1.04   fed 0.577795787
//Card 1.06   Card 1.05   Card 1.07   feg 0.567970599
//Card 1.06   Card 1.05   Card 1.13   fem 0.339130589
//Card 1.06   Card 1.05   Card 1.14   fen 0.039623239
//Card 1.06   Card 1.05   Card 1.16   fep 0.728107736
//Card 1.06   Card 1.05   Card 1.20   fet 0.983561668
//Card 1.06   Card 1.05   Card 1.24   fex 0.818699245
//Card 1.06   Card 1.09   Card 1.02   fib 0.045233657
//Card 1.06   Card 1.09   Card 1.03   fic 0.729219602
//Card 1.06   Card 1.09   Card 1.04   fid 0.372985862
//Card 1.06   Card 1.09   Card 1.07   fig 0.969453028
//Card 1.06   Card 1.09   Card 1.13   fim 0.983959278
//Card 1.06   Card 1.09   Card 1.14   fin 0.639056295
//Card 1.06   Card 1.09   Card 1.16   fip 0.705418813
//Card 1.06   Card 1.09   Card 1.20   fit 0.806955803
//Card 1.06   Card 1.09   Card 1.24   fix 0.094569514
//Card 1.06   Card 1.15   Card 1.02   fob 0.009560721
//Card 1.06   Card 1.15   Card 1.03   foc 0.833166605
//Card 1.06   Card 1.15   Card 1.04   fod 0.635329616
//Card 1.06   Card 1.15   Card 1.07   fog 0.078077076
//Card 1.06   Card 1.15   Card 1.13   fom 0.924820335
//Card 1.06   Card 1.15   Card 1.14   fon 0.025891057
//Card 1.06   Card 1.15   Card 1.16   fop 0.553485301
//Card 1.06   Card 1.15   Card 1.20   fot 0.610558281
//Card 1.06   Card 1.15   Card 1.24   fox 0.30308478
//Card 1.07   Card 1.01   Card 1.02   gab 0.464015878
//Card 1.07   Card 1.01   Card 1.03   gac 0.651758889
//Card 1.07   Card 1.01   Card 1.04   gad 0.840383328
//Card 1.07   Card 1.01   Card 1.07   gag 0.714146554
//Card 1.07   Card 1.01   Card 1.13   gam 0.12742399
//Card 1.07   Card 1.01   Card 1.14   gan 0.257777123
//Card 1.07   Card 1.01   Card 1.16   gap 0.264188801
//Card 1.07   Card 1.01   Card 1.20   gat 0.997388381
//Card 1.07   Card 1.01   Card 1.24   gax 0.008476559
//Card 1.07   Card 1.15   Card 1.02   gob 0.77998143
//Card 1.07   Card 1.15   Card 1.03   goc 0.715959048
//Card 1.07   Card 1.15   Card 1.04   god 0.046124017
//Card 1.07   Card 1.15   Card 1.07   gog 0.097312822
//Card 1.07   Card 1.15   Card 1.13   gom 0.415940359
//Card 1.07   Card 1.15   Card 1.14   gon 0.95196112
//Card 1.07   Card 1.15   Card 1.16   gop 0.938584035
//Card 1.07   Card 1.15   Card 1.20   got 0.701503899
//Card 1.07   Card 1.15   Card 1.24   gox 0.96435225
//Card 1.08   Card 1.01   Card 1.02   hab 0.617993939
//Card 1.08   Card 1.01   Card 1.03   hac 0.93139072
//Card 1.08   Card 1.01   Card 1.04   had 0.839986799
//Card 1.08   Card 1.01   Card 1.07   hag 0.216231311
//Card 1.08   Card 1.01   Card 1.13   ham 0.434931153
//Card 1.08   Card 1.01   Card 1.14   han 0.231726086
//Card 1.08   Card 1.01   Card 1.16   hap 0.817867019
//Card 1.08   Card 1.01   Card 1.20   hat 0.185043304
//Card 1.08   Card 1.01   Card 1.24   hax 0.652122065
//Card 1.08   Card 1.05   Card 1.02   heb 0.236762385
//Card 1.08   Card 1.05   Card 1.03   hec 0.654853977
//Card 1.08   Card 1.05   Card 1.04   hed 0.933300801
//Card 1.08   Card 1.05   Card 1.07   heg 0.147757326
//Card 1.08   Card 1.05   Card 1.13   hem 0.69492807
//Card 1.08   Card 1.05   Card 1.14   hen 0.864708179
//Card 1.08   Card 1.05   Card 1.16   hep 0.723641353
//Card 1.08   Card 1.05   Card 1.20   het 0.96260086
//Card 1.08   Card 1.05   Card 1.24   hex 0.301561418
//Card 1.08   Card 1.09   Card 1.02   hib 0.225075812
//Card 1.08   Card 1.09   Card 1.03   hic 0.708667613
//Card 1.08   Card 1.09   Card 1.04   hid 0.16516124
//Card 1.08   Card 1.09   Card 1.07   hig 0.540609335
//Card 1.08   Card 1.09   Card 1.13   him 0.345258164
//Card 1.08   Card 1.09   Card 1.14   hin 0.241600463
//Card 1.08   Card 1.09   Card 1.16   hip 0.839039654
//Card 1.08   Card 1.09   Card 1.20   hit 0.874993709
//Card 1.08   Card 1.09   Card 1.24   hix 0.029503277
//Card 1.08   Card 1.15   Card 1.02   hob 0.341686304
//Card 1.08   Card 1.15   Card 1.03   hoc 0.452900131
//Card 1.08   Card 1.15   Card 1.04   hod 0.030735723
//Card 1.08   Card 1.15   Card 1.07   hog 0.575385029
//Card 1.08   Card 1.15   Card 1.13   hom 0.798036352
//Card 1.08   Card 1.15   Card 1.14   hon 0.01028611
//Card 1.08   Card 1.15   Card 1.16   hop 0.250311088
//Card 1.08   Card 1.15   Card 1.20   hot 0.451944325
//Card 1.08   Card 1.15   Card 1.24   hox 0.945670801
//Card 1.10   Card 1.01   Card 1.02   jab 0.547806779
//Card 1.10   Card 1.01   Card 1.03   jac 0.284366648
//Card 1.10   Card 1.01   Card 1.04   jad 0.966584573
//Card 1.10   Card 1.01   Card 1.07   jag 0.562099687
//Card 1.10   Card 1.01   Card 1.13   jam 0.58866792
//Card 1.10   Card 1.01   Card 1.14   jan 0.58212521
//Card 1.10   Card 1.01   Card 1.16   jap 0.996280498
//Card 1.10   Card 1.01   Card 1.20   jat 0.520267916
//Card 1.10   Card 1.01   Card 1.24   jax 0.69172343
//Card 1.10   Card 1.05   Card 1.02   jeb 0.70676085
//Card 1.10   Card 1.05   Card 1.03   jec 0.762060668
//Card 1.10   Card 1.05   Card 1.04   jed 0.808913948
//Card 1.10   Card 1.05   Card 1.07   jeg 0.045971701
//Card 1.10   Card 1.05   Card 1.13   jem 0.142624958
//Card 1.10   Card 1.05   Card 1.14   jen 0.906112011
//Card 1.10   Card 1.05   Card 1.16   jep 0.072823155
//Card 1.10   Card 1.05   Card 1.20   jet 0.378795012
//Card 1.10   Card 1.05   Card 1.24   jex 0.840803916
//Card 1.10   Card 1.09   Card 1.02   jib 0.804955036
//Card 1.10   Card 1.09   Card 1.03   jic 0.993954995
//Card 1.10   Card 1.09   Card 1.04   jid 0.241735809
//Card 1.10   Card 1.09   Card 1.07   jig 0.992764776
//Card 1.10   Card 1.09   Card 1.13   jim 0.181538539
//Card 1.10   Card 1.09   Card 1.14   jin 0.86437087
//Card 1.10   Card 1.09   Card 1.16   jip 0.730935771
//Card 1.10   Card 1.09   Card 1.20   jit 0.255107501
//Card 1.10   Card 1.09   Card 1.24   jix 0.575977202
//Card 1.10   Card 1.15   Card 1.02   job 0.421902701
//Card 1.10   Card 1.15   Card 1.03   joc 0.214998859
//Card 1.10   Card 1.15   Card 1.04   jod 0.505402477
//Card 1.10   Card 1.15   Card 1.07   jog 0.274770236
//Card 1.10   Card 1.15   Card 1.13   jom 0.7337024
//Card 1.10   Card 1.15   Card 1.14   jon 0.026982822
//Card 1.10   Card 1.15   Card 1.16   jop 0.396980202
//Card 1.10   Card 1.15   Card 1.20   jot 0.968110421
//Card 1.10   Card 1.15   Card 1.24   jox 0.802349764
//Card 1.11   Card 1.01   Card 1.02   kab 0.746753329
//Card 1.11   Card 1.01   Card 1.03   kac 0.906648157
//Card 1.11   Card 1.01   Card 1.04   kad 0.255137623
//Card 1.11   Card 1.01   Card 1.07   kag 0.232075515
//Card 1.11   Card 1.01   Card 1.13   kam 0.404176706
//Card 1.11   Card 1.01   Card 1.14   kan 0.813339281
//Card 1.11   Card 1.01   Card 1.16   kap 0.499346192
//Card 1.11   Card 1.01   Card 1.20   kat 0.678422441
//Card 1.11   Card 1.01   Card 1.24   kax 0.931491478
//Card 1.11   Card 1.05   Card 1.02   keb 0.656073828
//Card 1.11   Card 1.05   Card 1.03   kec 0.270722761
//Card 1.11   Card 1.05   Card 1.04   ked 0.733703881
//Card 1.11   Card 1.05   Card 1.07   keg 0.421904338
//Card 1.11   Card 1.05   Card 1.13   kem 0.612249619
//Card 1.11   Card 1.05   Card 1.14   ken 0.63417503
//Card 1.11   Card 1.05   Card 1.16   kep 0.529402743
//Card 1.11   Card 1.05   Card 1.20   ket 0.946991209
//Card 1.11   Card 1.05   Card 1.24   kex 0.835556565
//Card 1.11   Card 1.09   Card 1.02   kib 0.95478125
//Card 1.11   Card 1.09   Card 1.03   kic 0.580338541
//Card 1.11   Card 1.09   Card 1.04   kid 0.515066171
//Card 1.11   Card 1.09   Card 1.07   kig 0.788322571
//Card 1.11   Card 1.09   Card 1.13   kim 0.426438039
//Card 1.11   Card 1.09   Card 1.14   kin 0.600637752
//Card 1.11   Card 1.09   Card 1.16   kip 0.48843114
//Card 1.11   Card 1.09   Card 1.20   kit 0.115629864
//Card 1.11   Card 1.09   Card 1.24   kix 0.04483919
//Card 1.11   Card 1.15   Card 1.02   kob 0.698636885
//Card 1.11   Card 1.15   Card 1.04   kod 0.714975215
//Card 1.11   Card 1.15   Card 1.07   kog 0.251548106
//Card 1.11   Card 1.15   Card 1.13   kom 0.747597161
//Card 1.11   Card 1.15   Card 1.14   kon 0.95717212
//Card 1.11   Card 1.15   Card 1.16   kop 0.663018793
//Card 1.11   Card 1.15   Card 1.20   kot 0.672309463
//Card 1.12   Card 1.01   Card 1.02   lab 0.804176231
//Card 1.12   Card 1.01   Card 1.03   lac 0.477440431
//Card 1.12   Card 1.01   Card 1.04   lad 0.496511019
//Card 1.12   Card 1.01   Card 1.07   lag 0.509755222
//Card 1.12   Card 1.01   Card 1.13   lam 0.118640855
//Card 1.12   Card 1.01   Card 1.14   lan 0.162388804
//Card 1.12   Card 1.01   Card 1.16   lap 0.464468217
//Card 1.12   Card 1.01   Card 1.20   lat 0.042043244
//Card 1.12   Card 1.01   Card 1.24   lax 0.019902321
//Card 1.12   Card 1.05   Card 1.02   leb 0.204356329
//Card 1.12   Card 1.05   Card 1.03   lec 0.474573905
//Card 1.12   Card 1.05   Card 1.04   led 0.021886804
//Card 1.12   Card 1.05   Card 1.07   leg 0.274205701
//Card 1.12   Card 1.05   Card 1.13   lem 0.292876675
//Card 1.12   Card 1.05   Card 1.14   len 0.706507636
//Card 1.12   Card 1.05   Card 1.16   lep 0.850434979
//Card 1.12   Card 1.05   Card 1.20   let 0.55339314
//Card 1.12   Card 1.05   Card 1.24   lex 0.196552302
//Card 1.12   Card 1.09   Card 1.02   lib 0.301511549
//Card 1.12   Card 1.09   Card 1.03   lic 0.546905852
//Card 1.12   Card 1.09   Card 1.04   lid 0.924779384
//Card 1.12   Card 1.09   Card 1.07   lig 0.051157014
//Card 1.12   Card 1.09   Card 1.13   lim 0.091027251
//Card 1.12   Card 1.09   Card 1.14   lin 0.25021319
//Card 1.12   Card 1.09   Card 1.16   lip 0.371559684
//Card 1.12   Card 1.09   Card 1.20   lit 0.97934588
//Card 1.12   Card 1.09   Card 1.24   lix 0.350327139
//Card 1.12   Card 1.15   Card 1.02   lob 0.464982654
//Card 1.12   Card 1.15   Card 1.03   loc 0.361588177
//Card 1.12   Card 1.15   Card 1.04   lod 0.442315588
//Card 1.12   Card 1.15   Card 1.07   log 0.738537456
//Card 1.12   Card 1.15   Card 1.13   lom 0.171577095
//Card 1.12   Card 1.15   Card 1.14   lon 0.621043794
//Card 1.12   Card 1.15   Card 1.16   lop 0.008263647
//Card 1.12   Card 1.15   Card 1.20   lot 0.031135199
//Card 1.12   Card 1.15   Card 1.24   lox 0.221393896
//Card 1.13   Card 1.01   Card 1.02   mab 0.225557397
//Card 1.13   Card 1.01   Card 1.03   mac 0.316687702
//Card 1.13   Card 1.01   Card 1.04   mad 0.697521603
//Card 1.13   Card 1.01   Card 1.07   mag 0.216995367
//Card 1.13   Card 1.01   Card 1.13   mam 0.023504168
//Card 1.13   Card 1.01   Card 1.14   man 0.330771964
//Card 1.13   Card 1.01   Card 1.16   map 0.528048858
//Card 1.13   Card 1.01   Card 1.20   mat 0.475784943
//Card 1.13   Card 1.01   Card 1.24   max 0.411446411
//Card 1.13   Card 1.05   Card 1.02   meb 0.935080048
//Card 1.13   Card 1.05   Card 1.03   mec 0.010699665
//Card 1.13   Card 1.05   Card 1.04   med 0.12574358
//Card 1.13   Card 1.05   Card 1.07   meg 0.378611248
//Card 1.13   Card 1.05   Card 1.13   mem 0.375386574
//Card 1.13   Card 1.05   Card 1.14   men 0.775696836
//Card 1.13   Card 1.05   Card 1.16   mep 0.215173944
//Card 1.13   Card 1.05   Card 1.20   met 0.478491546
//Card 1.13   Card 1.05   Card 1.24   mex 0.200745162
//Card 1.13   Card 1.09   Card 1.02   mib 0.33712491
//Card 1.13   Card 1.09   Card 1.03   mic 0.840901628
//Card 1.13   Card 1.09   Card 1.04   mid 0.902906964
//Card 1.13   Card 1.09   Card 1.07   mig 0.053889658
//Card 1.13   Card 1.09   Card 1.13   mim 0.042165308
//Card 1.13   Card 1.09   Card 1.14   min 0.846029555
//Card 1.13   Card 1.09   Card 1.16   mip 0.619789904
//Card 1.13   Card 1.09   Card 1.20   mit 0.209040135
//Card 1.13   Card 1.09   Card 1.24   mix 0.164481298
//Card 1.13   Card 1.15   Card 1.02   mob 0.884724619
//Card 1.13   Card 1.15   Card 1.03   moc 0.501321502
//Card 1.13   Card 1.15   Card 1.04   mod 0.189984581
//Card 1.13   Card 1.15   Card 1.07   mog 0.147077252
//Card 1.13   Card 1.15   Card 1.13   mom 0.500835393
//Card 1.13   Card 1.15   Card 1.14   mon 0.829978832
//Card 1.13   Card 1.15   Card 1.16   mop 0.48044503
//Card 1.13   Card 1.15   Card 1.20   mot 0.620864415
//Card 1.13   Card 1.15   Card 1.24   mox 0.013943605
//Card 1.14   Card 1.01   Card 1.02   nab 0.595245689
//Card 1.14   Card 1.01   Card 1.03   nac 0.003849461
//Card 1.14   Card 1.01   Card 1.04   nad 0.06265227
//Card 1.14   Card 1.01   Card 1.07   nag 0.31705747
//Card 1.14   Card 1.01   Card 1.13   nam 0.769046655
//Card 1.14   Card 1.01   Card 1.14   nan 0.012227344
//Card 1.14   Card 1.01   Card 1.16   nap 0.869032983
//Card 1.14   Card 1.01   Card 1.20   nat 0.757449073
//Card 1.14   Card 1.01   Card 1.24   nax 0.951664885
//Card 1.14   Card 1.05   Card 1.02   neb 0.407395653
//Card 1.14   Card 1.05   Card 1.03   nec 0.208345408
//Card 1.14   Card 1.05   Card 1.04   ned 0.631371372
//Card 1.14   Card 1.05   Card 1.07   neg 0.892870765
//Card 1.14   Card 1.05   Card 1.13   nem 0.313928517
//Card 1.14   Card 1.05   Card 1.14   nen 0.601696877
//Card 1.14   Card 1.05   Card 1.16   nep 0.899292758
//Card 1.14   Card 1.05   Card 1.20   net 0.731246512
//Card 1.14   Card 1.05   Card 1.24   nex 0.782081213
//Card 1.14   Card 1.09   Card 1.02   nib 0.254343209
//Card 1.14   Card 1.09   Card 1.03   nic 0.764175806
//Card 1.14   Card 1.09   Card 1.04   nid 0.717673664
//Card 1.14   Card 1.09   Card 1.07   nig 0.051295885
//Card 1.14   Card 1.09   Card 1.13   nim 0.926464432
//Card 1.14   Card 1.09   Card 1.14   nin 0.143999293
//Card 1.14   Card 1.09   Card 1.16   nip 0.70889456
//Card 1.14   Card 1.09   Card 1.20   nit 0.490764882
//Card 1.14   Card 1.09   Card 1.24   nix 0.570518042
//Card 1.14   Card 1.15   Card 1.02   nob 0.936121921
//Card 1.14   Card 1.15   Card 1.03   noc 0.162385659
//Card 1.14   Card 1.15   Card 1.04   nod 0.945511448
//Card 1.14   Card 1.15   Card 1.07   nog 0.625999275
//Card 1.14   Card 1.15   Card 1.13   nom 0.989509577
//Card 1.14   Card 1.15   Card 1.14   non 0.794047304
//Card 1.14   Card 1.15   Card 1.16   nop 0.608078832
//Card 1.14   Card 1.15   Card 1.20   not 0.486297033
//Card 1.14   Card 1.15   Card 1.24   nox 0.328244746
//Card 1.16   Card 1.01   Card 1.02   pab 0.458512793
//Card 1.16   Card 1.01   Card 1.03   pac 0.513351692
//Card 1.16   Card 1.01   Card 1.04   pad 0.156100318
//Card 1.16   Card 1.01   Card 1.07   pag 0.419571538
//Card 1.16   Card 1.01   Card 1.13   pam 0.079822203
//Card 1.16   Card 1.01   Card 1.14   pan 0.006366551
//Card 1.16   Card 1.01   Card 1.16   pap 0.057321259
//Card 1.16   Card 1.01   Card 1.20   pat 0.630631966
//Card 1.16   Card 1.01   Card 1.24   pax 0.48594373
//Card 1.16   Card 1.05   Card 1.02   peb 0.774491591
//Card 1.16   Card 1.05   Card 1.03   pec 0.213959745
//Card 1.16   Card 1.05   Card 1.04   ped 0.900755432
//Card 1.16   Card 1.05   Card 1.07   peg 0.300613974
//Card 1.16   Card 1.05   Card 1.13   pem 0.927357041
//Card 1.16   Card 1.05   Card 1.14   pen 0.883842061
//Card 1.16   Card 1.05   Card 1.16   pep 0.448876158
//Card 1.16   Card 1.05   Card 1.20   pet 0.719487051
//Card 1.16   Card 1.05   Card 1.24   pex 0.417445579
//Card 1.16   Card 1.09   Card 1.02   pib 0.738657984
//Card 1.16   Card 1.09   Card 1.03   pic 0.775224298
//Card 1.16   Card 1.09   Card 1.04   pid 0.766637993
//Card 1.16   Card 1.09   Card 1.07   pig 0.873590069
//Card 1.16   Card 1.09   Card 1.13   pim 0.843752459
//Card 1.16   Card 1.09   Card 1.14   pin 0.145530267
//Card 1.16   Card 1.09   Card 1.16   pip 0.554740758
//Card 1.16   Card 1.09   Card 1.20   pit 0.620117393
//Card 1.16   Card 1.09   Card 1.24   pix 0.835266558
//Card 1.16   Card 1.15   Card 1.02   pob 0.589864681
//Card 1.16   Card 1.15   Card 1.03   poc 0.572590951
//Card 1.16   Card 1.15   Card 1.04   pod 0.066320363
//Card 1.16   Card 1.15   Card 1.07   pog 0.686754283
//Card 1.16   Card 1.15   Card 1.13   pom 0.280043601
//Card 1.16   Card 1.15   Card 1.14   pon 0.876205915
//Card 1.16   Card 1.15   Card 1.16   pop 0.60029072
//Card 1.16   Card 1.15   Card 1.20   pot 0.100632454
//Card 1.16   Card 1.15   Card 1.24   pox 0.226516014
//Card 1.17   Card 1.01   Card 1.02   quab    0.373859314
//Card 1.17   Card 1.01   Card 1.03   quac    0.423938203
//Card 1.17   Card 1.01   Card 1.04   quad    0.933719552
//Card 1.17   Card 1.01   Card 1.07   quag    0.382269641
//Card 1.17   Card 1.01   Card 1.13   quam    0.728709387
//Card 1.17   Card 1.01   Card 1.14   quan    0.022229444
//Card 1.17   Card 1.01   Card 1.16   quap    0.104733241
//Card 1.17   Card 1.01   Card 1.20   quat    0.015021573
//Card 1.17   Card 1.01   Card 1.24   quax    0.574660023
//Card 1.17   Card 1.05   Card 1.02   queb    0.773893248
//Card 1.17   Card 1.05   Card 1.03   quec    0.3080569
//Card 1.17   Card 1.05   Card 1.04   qued    0.302574977
//Card 1.17   Card 1.05   Card 1.07   queg    0.232889851
//Card 1.17   Card 1.05   Card 1.13   quem    0.206051776
//Card 1.17   Card 1.05   Card 1.14   quen    0.118835235
//Card 1.17   Card 1.05   Card 1.16   quep    0.504934718
//Card 1.17   Card 1.05   Card 1.20   quet    0.990897373
//Card 1.17   Card 1.05   Card 1.24   quex    0.767954889
//Card 1.17   Card 1.09   Card 1.02   quib    0.40299453
//Card 1.17   Card 1.09   Card 1.03   quic    0.687752089
//Card 1.17   Card 1.09   Card 1.04   quid    0.517917418
//Card 1.17   Card 1.09   Card 1.07   quig    0.060788076
//Card 1.17   Card 1.09   Card 1.13   quim    0.318589373
//Card 1.17   Card 1.09   Card 1.14   quin    0.503328539
//Card 1.17   Card 1.09   Card 1.16   quip    0.895875164
//Card 1.17   Card 1.09   Card 1.20   quit    0.933224441
//Card 1.17   Card 1.09   Card 1.24   quix    0.001581925
//Card 1.17   Card 1.15   Card 1.02   quob    0.322295083
//Card 1.17   Card 1.15   Card 1.03   quoc    0.393558152
//Card 1.17   Card 1.15   Card 1.04   quod    0.43389265
//Card 1.17   Card 1.15   Card 1.07   quog    0.227638396
//Card 1.17   Card 1.15   Card 1.13   quom    0.276166486
//Card 1.17   Card 1.15   Card 1.14   quon    0.828602649
//Card 1.17   Card 1.15   Card 1.16   quop    0.164248723
//Card 1.17   Card 1.15   Card 1.20   quot    0.431128775
//Card 1.17   Card 1.15   Card 1.24   quox    0.983700131
//Card 1.18   Card 1.01   Card 1.02   rab 0.496605807
//Card 1.18   Card 1.01   Card 1.03   rac 0.982294809
//Card 1.18   Card 1.01   Card 1.04   rad 0.938389546
//Card 1.18   Card 1.01   Card 1.07   rag 0.101841914
//Card 1.18   Card 1.01   Card 1.13   ram 0.096688937
//Card 1.18   Card 1.01   Card 1.14   ran 0.66161433
//Card 1.18   Card 1.01   Card 1.16   rap 0.522207503
//Card 1.18   Card 1.01   Card 1.20   rat 0.289152038
//Card 1.18   Card 1.01   Card 1.24   rax 0.536592147
//Card 1.18   Card 1.05   Card 1.02   reb 0.8565815
//Card 1.18   Card 1.05   Card 1.03   rec 0.536957917
//Card 1.18   Card 1.05   Card 1.04   red 0.87620149
//Card 1.18   Card 1.05   Card 1.07   reg 0.538042739
//Card 1.18   Card 1.05   Card 1.13   rem 0.575371273
//Card 1.18   Card 1.05   Card 1.14   ren 0.753388289
//Card 1.18   Card 1.05   Card 1.16   rep 0.852902828
//Card 1.18   Card 1.05   Card 1.20   ret 0.858701247
//Card 1.18   Card 1.05   Card 1.24   rex 0.064553023
//Card 1.18   Card 1.09   Card 1.02   rib 0.146441657
//Card 1.18   Card 1.09   Card 1.03   ric 0.070803578
//Card 1.18   Card 1.09   Card 1.04   rid 0.797228321
//Card 1.18   Card 1.09   Card 1.07   rig 0.571724429
//Card 1.18   Card 1.09   Card 1.13   rim 0.897281669
//Card 1.18   Card 1.09   Card 1.14   rin 0.02390428
//Card 1.18   Card 1.09   Card 1.16   rip 0.11858828
//Card 1.18   Card 1.09   Card 1.20   rit 0.927584567
//Card 1.18   Card 1.09   Card 1.24   rix 0.815841197
//Card 1.18   Card 1.15   Card 1.02   rob 0.034286111
//Card 1.18   Card 1.15   Card 1.03   roc 0.739448519
//Card 1.18   Card 1.15   Card 1.04   rod 0.918304683
//Card 1.18   Card 1.15   Card 1.07   rog 0.099244197
//Card 1.18   Card 1.15   Card 1.13   rom 0.790563691
//Card 1.18   Card 1.15   Card 1.14   ron 0.736296177
//Card 1.18   Card 1.15   Card 1.16   rop 0.822362226
//Card 1.18   Card 1.15   Card 1.20   rot 0.707726814
//Card 1.18   Card 1.15   Card 1.24   rox 0.456315358
//Card 1.19   Card 1.01   Card 1.02   sab 0.917928045
//Card 1.19   Card 1.01   Card 1.03   sac 0.141383509
//Card 1.19   Card 1.01   Card 1.04   sad 0.568644507
//Card 1.19   Card 1.01   Card 1.07   sag 0.858577924
//Card 1.19   Card 1.01   Card 1.13   sam 0.822092364
//Card 1.19   Card 1.01   Card 1.14   san 0.782002771
//Card 1.19   Card 1.01   Card 1.16   sap 0.896481545
//Card 1.19   Card 1.01   Card 1.20   sat 0.458532771
//Card 1.19   Card 1.01   Card 1.24   sax 0.029152047
//Card 1.19   Card 1.05   Card 1.02   seb 0.334309419
//Card 1.19   Card 1.05   Card 1.03   sec 0.384603848
//Card 1.19   Card 1.05   Card 1.04   sed 0.150200048
//Card 1.19   Card 1.05   Card 1.07   seg 0.866009734
//Card 1.19   Card 1.05   Card 1.13   sem 0.480154496
//Card 1.19   Card 1.05   Card 1.14   sen 0.244180951
//Card 1.19   Card 1.05   Card 1.16   sep 0.990030988
//Card 1.19   Card 1.05   Card 1.20   set 0.353569111
//Card 1.19   Card 1.09   Card 1.02   sib 0.350772257
//Card 1.19   Card 1.09   Card 1.03   sic 0.913490574
//Card 1.19   Card 1.09   Card 1.04   sid 0.0889354
//Card 1.19   Card 1.09   Card 1.07   sig 0.473289559
//Card 1.19   Card 1.09   Card 1.13   sim 0.441095118
//Card 1.19   Card 1.09   Card 1.14   sin 0.062811656
//Card 1.19   Card 1.09   Card 1.16   sip 0.894069965
//Card 1.19   Card 1.09   Card 1.20   sit 0.709281485
//Card 1.19   Card 1.09   Card 1.24   six 0.350927623
//Card 1.19   Card 1.15   Card 1.03   soc 0.610635299
//Card 1.19   Card 1.15   Card 1.04   sod 0.657606108
//Card 1.19   Card 1.15   Card 1.07   sog 0.540778531
//Card 1.19   Card 1.15   Card 1.13   som 0.362070095
//Card 1.19   Card 1.15   Card 1.16   sop 0.615964953
//Card 1.19   Card 1.15   Card 1.20   sot 0.153122241
//Card 1.19   Card 1.15   Card 1.24   sox 0.081162136
//Card 1.20   Card 1.01   Card 1.02   tab 0.698494719
//Card 1.20   Card 1.01   Card 1.03   tac 0.392268649
//Card 1.20   Card 1.01   Card 1.04   tad 0.976812199
//Card 1.20   Card 1.01   Card 1.07   tag 0.454211681
//Card 1.20   Card 1.01   Card 1.13   tam 0.64396702
//Card 1.20   Card 1.01   Card 1.14   tan 0.495550512
//Card 1.20   Card 1.01   Card 1.16   tap 0.974069407
//Card 1.20   Card 1.01   Card 1.20   tat 0.708799518
//Card 1.20   Card 1.01   Card 1.24   tax 0.667637395
//Card 1.20   Card 1.05   Card 1.02   teb 0.781671764
//Card 1.20   Card 1.05   Card 1.03   tec 0.345414582
//Card 1.20   Card 1.05   Card 1.04   ted 0.01357443
//Card 1.20   Card 1.05   Card 1.07   teg 0.169962799
//Card 1.20   Card 1.05   Card 1.13   tem 0.340920397
//Card 1.20   Card 1.05   Card 1.14   ten 0.067132517
//Card 1.20   Card 1.05   Card 1.16   tep 0.950117676
//Card 1.20   Card 1.05   Card 1.20   tet 0.468738481
//Card 1.20   Card 1.05   Card 1.24   tex 0.440728167
//Card 1.20   Card 1.09   Card 1.02   tib 0.748003634
//Card 1.20   Card 1.09   Card 1.03   tic 0.799157432
//Card 1.20   Card 1.09   Card 1.04   tid 0.684219075
//Card 1.20   Card 1.09   Card 1.07   tig 0.71700972
//Card 1.20   Card 1.09   Card 1.13   tim 0.789751967
//Card 1.20   Card 1.09   Card 1.14   tin 0.476770788
//Card 1.20   Card 1.09   Card 1.16   tip 0.006905245
//Card 1.20   Card 1.09   Card 1.20   tit 0.852721537
//Card 1.20   Card 1.09   Card 1.24   tix 0.929270462
//Card 1.20   Card 1.15   Card 1.02   tob 0.581379764
//Card 1.20   Card 1.15   Card 1.03   toc 0.307029017
//Card 1.20   Card 1.15   Card 1.04   tod 0.907235287
//Card 1.20   Card 1.15   Card 1.07   tog 0.568196998
//Card 1.20   Card 1.15   Card 1.13   tom 0.062674142
//Card 1.20   Card 1.15   Card 1.14   ton 0.266870662
//Card 1.20   Card 1.15   Card 1.16   top 0.481056966
//Card 1.20   Card 1.15   Card 1.20   tot 0.829754464
//Card 1.20   Card 1.15   Card 1.24   tox 0.568299082
//Card 1.22   Card 1.01   Card 1.02   vab 0.475733192
//Card 1.22   Card 1.01   Card 1.03   vac 0.886289119
//Card 1.22   Card 1.01   Card 1.04   vad 0.975542396
//Card 1.22   Card 1.01   Card 1.07   vag 0.68089069
//Card 1.22   Card 1.01   Card 1.13   vam 0.543947461
//Card 1.22   Card 1.01   Card 1.14   van 0.565158255
//Card 1.22   Card 1.01   Card 1.16   vap 0.683198344
//Card 1.22   Card 1.01   Card 1.20   vat 0.506029385
//Card 1.22   Card 1.01   Card 1.24   vax 0.591392336
//Card 1.22   Card 1.05   Card 1.02   veb 0.607833161
//Card 1.22   Card 1.05   Card 1.03   vec 0.961011186
//Card 1.22   Card 1.05   Card 1.04   ved 0.876799608
//Card 1.22   Card 1.05   Card 1.07   veg 0.321763963
//Card 1.22   Card 1.05   Card 1.13   vem 0.04884676
//Card 1.22   Card 1.05   Card 1.14   ven 0.829994518
//Card 1.22   Card 1.05   Card 1.16   vep 0.129972854
//Card 1.22   Card 1.05   Card 1.20   vet 0.83060436
//Card 1.22   Card 1.05   Card 1.24   vex 0.245343553
//Card 1.22   Card 1.09   Card 1.02   vib 0.836981593
//Card 1.22   Card 1.09   Card 1.03   vic 0.823057484
//Card 1.22   Card 1.09   Card 1.04   vid 0.075112634
//Card 1.22   Card 1.09   Card 1.07   vig 0.679303323
//Card 1.22   Card 1.09   Card 1.13   vim 0.316068162
//Card 1.22   Card 1.09   Card 1.14   vin 0.538435057
//Card 1.22   Card 1.09   Card 1.16   vip 0.12384699
//Card 1.22   Card 1.09   Card 1.20   vit 0.327742653
//Card 1.22   Card 1.09   Card 1.24   vix 0.451349975
//Card 1.22   Card 1.15   Card 1.02   vob 0.305282593
//Card 1.22   Card 1.15   Card 1.03   voc 0.051626631
//Card 1.22   Card 1.15   Card 1.04   vod 0.815516234
//Card 1.22   Card 1.15   Card 1.07   vog 0.119059164
//Card 1.22   Card 1.15   Card 1.13   vom 0.265841388
//Card 1.22   Card 1.15   Card 1.14   von 0.976407794
//Card 1.22   Card 1.15   Card 1.16   vop 0.741069301
//Card 1.22   Card 1.15   Card 1.20   vot 0.98186212
//Card 1.22   Card 1.15   Card 1.24   vox 0.381731865
//Card 1.23   Card 1.05   Card 1.02   web 0.681782143
//Card 1.23   Card 1.05   Card 1.03   wec 0.262737461
//Card 1.23   Card 1.05   Card 1.04   wed 0.099942536
//Card 1.23   Card 1.05   Card 1.07   weg 0.420425347
//Card 1.23   Card 1.05   Card 1.13   wem 0.826937878
//Card 1.23   Card 1.05   Card 1.14   wen 0.313563485
//Card 1.23   Card 1.05   Card 1.16   wep 0.310273659
//Card 1.23   Card 1.05   Card 1.20   wet 0.453100841
//Card 1.23   Card 1.05   Card 1.24   wex 0.365321126
//Card 1.23   Card 1.09   Card 1.02   wib 0.530205625
//Card 1.23   Card 1.09   Card 1.03   wic 0.396037264
//Card 1.23   Card 1.09   Card 1.04   wid 0.296074641
//Card 1.23   Card 1.09   Card 1.07   wig 0.648244995
//Card 1.23   Card 1.09   Card 1.13   wim 0.461949929
//Card 1.23   Card 1.09   Card 1.14   win 0.880322177
//Card 1.23   Card 1.09   Card 1.16   wip 0.347302939
//Card 1.23   Card 1.09   Card 1.20   wit 0.232808949
//Card 1.23   Card 1.09   Card 1.24   wix 0.421032503
//Card 1.23   Card 1.15   Card 1.02   wob 0.041884345
//Card 1.23   Card 1.15   Card 1.03   woc 0.300218478
//Card 1.23   Card 1.15   Card 1.04   wod 0.426031763
//Card 1.23   Card 1.15   Card 1.07   wog 0.644873243
//Card 1.23   Card 1.15   Card 1.13   wom 0.162868506
//Card 1.23   Card 1.15   Card 1.16   wop 0.869210296
//Card 1.23   Card 1.15   Card 1.20   wot 0.258089856
//Card 1.23   Card 1.15   Card 1.24   wox 0.948560489
//Card 1.25   Card 1.01   Card 1.02   yab 0.903525148
//Card 1.25   Card 1.01   Card 1.03   yac 0.839658271
//Card 1.25   Card 1.01   Card 1.04   yad 0.350197832
//Card 1.25   Card 1.01   Card 1.07   yag 0.617963752
//Card 1.25   Card 1.01   Card 1.13   yam 0.939113748
//Card 1.25   Card 1.01   Card 1.14   yan 0.664269018
//Card 1.25   Card 1.01   Card 1.16   yap 0.146180811
//Card 1.25   Card 1.01   Card 1.20   yat 0.292486297
//Card 1.25   Card 1.01   Card 1.24   yax 0.3433397
//Card 1.25   Card 1.05   Card 1.02   yeb 0.719936568
//Card 1.25   Card 1.05   Card 1.03   yec 0.959082519
//Card 1.25   Card 1.05   Card 1.04   yed 0.578584397
//Card 1.25   Card 1.05   Card 1.07   yeg 0.36824669
//Card 1.25   Card 1.05   Card 1.13   yem 0.327276198
//Card 1.25   Card 1.05   Card 1.14   yen 0.483830845
//Card 1.25   Card 1.05   Card 1.16   yep 0.223753649
//Card 1.25   Card 1.05   Card 1.20   yet 0.989150505
//Card 1.25   Card 1.05   Card 1.24   yex 0.268670828
//Card 1.25   Card 1.09   Card 1.02   yib 0.355841225
//Card 1.25   Card 1.09   Card 1.03   yic 0.264705468
//Card 1.25   Card 1.09   Card 1.04   yid 0.530629302
//Card 1.25   Card 1.09   Card 1.07   yig 0.019817404
//Card 1.25   Card 1.09   Card 1.13   yim 0.916578724
//Card 1.25   Card 1.09   Card 1.14   yin 0.396457087
//Card 1.25   Card 1.09   Card 1.16   yip 0.416063166
//Card 1.25   Card 1.09   Card 1.20   yit 0.26841825
//Card 1.25   Card 1.09   Card 1.24   yix 0.879673131
//Card 1.25   Card 1.15   Card 1.02   yob 0.853777897
//Card 1.25   Card 1.15   Card 1.03   yoc 0.652258177
//Card 1.25   Card 1.15   Card 1.04   yod 0.930080959
//Card 1.25   Card 1.15   Card 1.07   yog 0.086889881
//Card 1.25   Card 1.15   Card 1.13   yom 0.257614197
//Card 1.25   Card 1.15   Card 1.14   yon 0.488726299
//Card 1.25   Card 1.15   Card 1.16   yop 0.176647073
//Card 1.25   Card 1.15   Card 1.20   yot 0.14653698
//Card 1.25   Card 1.15   Card 1.24   yox 0.169015298
//Card 1.26   Card 1.01   Card 1.02   zab 0.001395224
//Card 1.26   Card 1.01   Card 1.03   zac 0.517778912
//Card 1.26   Card 1.01   Card 1.04   zad 0.302882193
//Card 1.26   Card 1.01   Card 1.07   zag 0.367037405
//Card 1.26   Card 1.01   Card 1.13   zam 0.252677886
//Card 1.26   Card 1.01   Card 1.14   zan 0.15681754
//Card 1.26   Card 1.01   Card 1.16   zap 0.114375625
//Card 1.26   Card 1.01   Card 1.20   zat 0.185745844
//Card 1.26   Card 1.01   Card 1.24   zax 0.360355475
//Card 1.26   Card 1.05   Card 1.02   zeb 0.326140448
//Card 1.26   Card 1.05   Card 1.03   zec 0.193861242
//Card 1.26   Card 1.05   Card 1.04   zed 0.437802668
//Card 1.26   Card 1.05   Card 1.07   zeg 0.903049275
//Card 1.26   Card 1.05   Card 1.13   zem 0.087133674
//Card 1.26   Card 1.05   Card 1.14   zen 0.760193968
//Card 1.26   Card 1.05   Card 1.16   zep 0.183295872
//Card 1.26   Card 1.05   Card 1.20   zet 0.510957059
//Card 1.26   Card 1.05   Card 1.24   zex 0.650558988
//Card 1.26   Card 1.09   Card 1.02   zib 0.182348917
//Card 1.26   Card 1.09   Card 1.03   zic 0.539387981
//Card 1.26   Card 1.09   Card 1.04   zid 0.250803139
//Card 1.26   Card 1.09   Card 1.07   zig 0.387617334
//Card 1.26   Card 1.09   Card 1.13   zim 0.933726682
//Card 1.26   Card 1.09   Card 1.14   zin 0.908716973
//Card 1.26   Card 1.09   Card 1.16   zip 0.031583838
//Card 1.26   Card 1.09   Card 1.20   zit 0.332564345
//Card 1.26   Card 1.09   Card 1.24   zix 0.056459985
//Card 1.26   Card 1.15   Card 1.02   zob 0.042060694
//Card 1.26   Card 1.15   Card 1.03   zoc 0.818056321
//Card 1.26   Card 1.15   Card 1.04   zod 0.802254656
//Card 1.26   Card 1.15   Card 1.07   zog 0.349614816
//Card 1.26   Card 1.15   Card 1.13   zom 0.536518686
//Card 1.26   Card 1.15   Card 1.14   zon 0.779068425
//Card 1.26   Card 1.15   Card 1.16   zop 0.205923754
//Card 1.26   Card 1.15   Card 1.20   zot 0.080982699
//Card 1.26   Card 1.15   Card 1.24   zox 0.020622151
        }

        private static void SeedTraits(IDatabaseContext context)
        {
            context.Traits.Add(Trait.GetTrait(1, TraitNameEnum.Closed_syllable, TraitDescEnum.a));
        }
    }
}
