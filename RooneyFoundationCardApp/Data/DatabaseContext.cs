﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Options;

namespace RooneyFoundationCardApp.Data
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public static string DatabaseName => "rooney.db";
        public static IDatabaseContext GetContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            optionsBuilder.UseSqlite($"Data Source={DatabaseName}");
            var context = new DatabaseContext(optionsBuilder.Options);

            return context;
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Trait> Traits { get; set; }
        public DbSet<Word> Words { get; set; }

        //Exacmple of mapping
        //protected override void OnModelCreating(ModelBuilder modelBuilder) 
        //{
        // modelBuilder.Entity<OrderDetail>().HasKey(p => new { p.OrderID, p.ProductID });
        //} 


    }

    public interface IDatabaseContext
    {
        DbSet<Card> Cards { get; set; }
        DbSet<Trait> Traits { get; set; }
        DbSet<Word> Words { get; set; }

        int SaveChanges(bool acceptAllChangesOnSuccess);
        DatabaseFacade Database { get; }
    }
}