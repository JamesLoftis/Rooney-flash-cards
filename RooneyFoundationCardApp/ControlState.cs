﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RooneyFoundationCardApp
{
    public static class ControlState
    {
        public static ButtonState CurrentLeftMouseState { get; set; }
        public static ButtonState PreviousLeftMouseState { get; set; }
        public static ButtonState CurrentMiddleMouseState { get; private set; }
        public static ButtonState PreviousMiddleMouseState { get; private set; }
        public static ButtonState CurrentRightMouseState { get; private set; }
        public static ButtonState PreviousRightMouseState { get; private set; }
        public static KeyboardState CurrentKeyboardState { get; private set; }
        public static KeyboardState PreviousKeyboardState { get; private set; }
        public static Point PreviousMoussePosition { get; private set; }
        public static Point CurrentMousePosition { get; private set; }


        public static void UpdateCurrent()
        {
            var mouseState = Mouse.GetState();
            CurrentLeftMouseState = mouseState.LeftButton;
            CurrentMiddleMouseState = mouseState.MiddleButton;
            CurrentRightMouseState = mouseState.RightButton;
            CurrentKeyboardState = Keyboard.GetState();
            CurrentMousePosition = new Point(mouseState.X, mouseState.Y);

        }

        public static void UpdatePrevious()
        {
            PreviousLeftMouseState = CurrentLeftMouseState;
            PreviousKeyboardState = CurrentKeyboardState;
            PreviousLeftMouseState = CurrentLeftMouseState;
            PreviousMiddleMouseState = CurrentMiddleMouseState;
            PreviousRightMouseState = CurrentRightMouseState;
            PreviousMoussePosition = CurrentMousePosition;
        }

        public static Point MouseLocation => CurrentMousePosition;
        public static bool IsKeyPressed(this Keys key) => PreviousKeyboardState.IsKeyDown(key) && CurrentKeyboardState.IsKeyUp(key);
        public static bool LeftMouseButtonClicked => CurrentLeftMouseState == ButtonState.Released && PreviousLeftMouseState == ButtonState.Pressed;
        public static bool MiddleMouseButtonClicked => CurrentMiddleMouseState == ButtonState.Released && PreviousMiddleMouseState == ButtonState.Pressed;
        public static bool RightMouseButtonClicked => CurrentRightMouseState == ButtonState.Released && PreviousRightMouseState == ButtonState.Pressed;
    }
}
