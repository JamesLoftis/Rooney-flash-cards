﻿                                     using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RooneyFoundationCardApp.Data;
using RooneyFoundationCardApp.Extensions;

namespace RooneyFoundationCardApp
{
    public class WordManager
    {
        IDatabaseContext _context;
        IList<Word> _shuffledList;
        int _selectedWordId;
        int[] _nextWords;
        int[] cards;
        public WordManager(IDatabaseContext context)
        {
            cards = new int[3];
            _nextWords = new int[3];
            _context = context;
            _shuffledList = _context.Words.ToList();
            _shuffledList.Shuffle();

            SelectWord(0);
            GetAllNext();
        }

        public Word GetSelectedWord() => _shuffledList[_selectedWordId];

        public void CardSelected (int cardSelected)
        {
            SelectWord(_nextWords[cardSelected]);
            GetAllNext();
        }
        private void GetAllNext()
        {
            for (var i = 0; i < 3; i++)
            {
                _nextWords[i] = GetNext(i);
            }
        }

        public void SelectWord(int cardSelected)
        {
            _selectedWordId = cardSelected;
            cards = _shuffledList[_selectedWordId].Cards;
        }

        public int GetNext(int staticCard)
        {
            for (var i = _selectedWordId + 1; i <= _shuffledList.Count; i++)
            {
                if(i == _shuffledList.Count)
                {
                    i = 0;
                }
                var selected = _shuffledList[i];
                bool match = true;
                for (var j = 0; j < 3; j++)
                {
                    if(j == staticCard)
                    {
                        continue;
                    }
                    match = match && selected.Cards[j] == cards[j];
                }
                if(match)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
