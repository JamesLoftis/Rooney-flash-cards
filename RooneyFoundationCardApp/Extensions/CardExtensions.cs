﻿using System;
using System.Collections.Generic;
using System.Linq;
using RooneyFoundationCardApp.Data;

namespace RooneyFoundationCardApp.Extensions
{
    public static class CardExtensions
    {
        public static int GetCardId(this IDatabaseContext context, string cardName) => context.Cards.First(x => x.Name == cardName).Id;
        public static Card GetCardById(this IDatabaseContext context, int cardId) => context.Cards.First(x => x.Id == cardId);

        public static bool IsMouseOver(this CardLocation location) => location.Rect.Contains(ControlState.MouseLocation);
        private static Random rng = new Random(); 
        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

       

        public static TraitNameEnum GetTraitName(this Trait trait)
        {
            return (TraitNameEnum)Enum.Parse(typeof(TraitNameEnum), trait.Name);
        }

        public static TraitDescEnum GetTraitDescription(this Trait trait)
        {
            return (TraitDescEnum)Enum.Parse(typeof(TraitDescEnum), trait.Description);
        }

    }
}
