﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RooneyFoundationCardApp
{
    public class CardLocation
    {
        public Rectangle Rect { get; private set; }
        public bool IsVisible { get; set; }
        public float X => Rect.X;
        public float Y => Rect.Y;
        public Point Location => Rect.Location;
        public Card SelectedCard {get; set;}
        public Texture2D Texture => SelectedCard.Texture;

        public CardLocation(Point location, Point size)
        {
            Rect = new Rectangle(location, size);
            IsVisible = true;
        }
    }
}
