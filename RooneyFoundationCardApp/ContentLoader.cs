﻿using System;
using Microsoft.Xna.Framework.Content;

namespace RooneyFoundationCardApp
{
    public static class ContentLoader
    {
        private static ContentManager _content;

        public static ContentManager Content
        {
            set
            {
                _content = value;
            }
        }

        public static T LoadContent<T>(this string textureMap)
        {
            return _content.Load<T>(textureMap);
        }
    }
}
