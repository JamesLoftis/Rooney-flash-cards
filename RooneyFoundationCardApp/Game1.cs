﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RooneyFoundationCardApp.Data;
using RooneyFoundationCardApp.Extensions;

namespace RooneyFoundationCardApp.Desktop
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Rectangle _destionationRectangle;

        CardLocation[] _cardLocations;
        WordManager _wordManager;
        IDatabaseContext _context;
        public Game1()
        {
            _context = DatabaseContext.GetContext();
            Seed.DoSeed(_context);
            _cardLocations = new CardLocation[3];
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            ContentLoader.Content = Content;
            _wordManager = new WordManager(_context);
            _destionationRectangle = new Rectangle(0, 0, 485, 630);
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            Window.AllowUserResizing = true;

        }
        Viewport _viewPort;
        int centerHeight(float yLocation)
        {
            var deadCenter =_viewPort.Height / 2;
            var halfY = yLocation / 2;
            return Convert.ToInt32(deadCenter - halfY);;
        }

        Color background;
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            graphics.PreferredBackBufferWidth = _destionationRectangle.Width * 3 + 60;  // set this value to the desired width of your window
            graphics.PreferredBackBufferHeight = _destionationRectangle.Height + 250;   // set this value to the desired height of your window
            //graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            _viewPort = graphics.GraphicsDevice.Viewport;

            //Textures.Load(Content);

            background = Color.CornflowerBlue;
            int cardVertical = centerHeight(_destionationRectangle.Height);
            _cardLocations[0] = new CardLocation(new Point(15, cardVertical), _destionationRectangle.Size);
            _cardLocations[1] = new CardLocation(new Point(30 + _destionationRectangle.Width, cardVertical), _destionationRectangle.Size);
            _cardLocations[2] = new CardLocation(new Point(45 + _destionationRectangle.Width * 2, cardVertical), _destionationRectangle.Size);

            UpdateTextures();

            //_cardLocations[0].Texture = _deck.Collection.First(x => x.Name == WordManager.CurrentWord.Columns[0]).Texture;
            //_cardLocations[1].Texture = _deck.Collection.First(x => x.Name == WordManager.CurrentWord.Columns[1]).Texture;
            //_cardLocations[2].Texture = _deck.Collection.First(x => x.Name == WordManager.CurrentWord.Columns[2]).Texture;
        }

        private void UpdateTextures()
        {
            var selectedWord = _wordManager.GetSelectedWord();
            for (var i = 0; i < 3; i++)
            {
                _cardLocations[i].SelectedCard = _context.GetCardById(selectedWord.Cards[i]);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            ControlState.UpdateCurrent();

            if (Keys.Escape.IsKeyPressed()) Exit();

            if(ControlState.LeftMouseButtonClicked)
            {
                var cardSelected = -1;
                if(_cardLocations[0].IsMouseOver())
                {
                    cardSelected = 0;
                }
                else if(_cardLocations[1].IsMouseOver())
                {
                    cardSelected  = 1;
                }
                else if(_cardLocations[2].IsMouseOver())
                {
                    cardSelected = 2;
                }
                if (cardSelected < 0) return;

                _wordManager.CardSelected(cardSelected);
            }
            UpdateTextures();
            ControlState.UpdatePrevious();
            base.Update(gameTime);
        }

  

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(background);
            spriteBatch.Begin();

            _destionationRectangle.X = Convert.ToInt32(_cardLocations[0].X);
            _destionationRectangle.Y = Convert.ToInt32(_cardLocations[1].Y);
            if(_cardLocations[0].IsVisible) spriteBatch.Draw(_cardLocations[0].Texture, _destionationRectangle, Color.White);
            _destionationRectangle.X = Convert.ToInt32(_cardLocations[1].X);
            _destionationRectangle.Y = Convert.ToInt32(_cardLocations[1].Y);
            if (_cardLocations[1].IsVisible) spriteBatch.Draw(_cardLocations[1].Texture, _destionationRectangle, Color.White);
            _destionationRectangle.X = Convert.ToInt32(_cardLocations[2].X);
            _destionationRectangle.Y = Convert.ToInt32(_cardLocations[2].Y);
            if (_cardLocations[2].IsVisible) spriteBatch.Draw(_cardLocations[2].Texture, _destionationRectangle, Color.White);

            //base.Draw(gameTime);
            spriteBatch.End();
        }
    }
}
