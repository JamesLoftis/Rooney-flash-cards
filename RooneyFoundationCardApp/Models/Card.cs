﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using Microsoft.Xna.Framework.Graphics;

namespace RooneyFoundationCardApp
{
    [Table(Name = "Cards")]
    public partial class Card
    {
        public Card(string name, string textureMap)
        {
            Name = name;
            TextureMap = textureMap;
        }

        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int Id { get; set; }

        [Column(Name = "Name", DbType = "VARCHAR")]
        public string Name { get; set; }

        [Column(Name = "TextureMap", DbType = "VARCHAR")]
        public string TextureMap { get; set; }
    }

    public partial class Card
    {
        public Texture2D _texture;
        public Texture2D Texture 
        { 
            get
            {
                if(_texture == null)
                {
                    _texture = ContentLoader.LoadContent<Texture2D>(TextureMap);
                }
                return _texture;
            } 
        }
    }
}