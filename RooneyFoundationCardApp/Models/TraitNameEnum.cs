﻿namespace RooneyFoundationCardApp
{
    public enum TraitNameEnum
    {
        Closed_syllable,
        Open_syllable,
        Magic_e,
        Magic_e_soft_c_and_g,
        r_comtrolled,
        Short_vowel_exceptions,
        Vowel_team
    }
}