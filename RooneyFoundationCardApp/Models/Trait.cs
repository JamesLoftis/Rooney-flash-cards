﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using Microsoft.Xna.Framework.Graphics;

namespace RooneyFoundationCardApp
{
    [Table(Name = "Traits")]
    public partial class Trait
    {
        public Trait(int cardId, string name, string desc) : this()
        {
            CardId = cardId;
            Name = name;
            Description = desc;
        }

        public Trait()
        {
            
        }

        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int Id { get; set; }

        [Column(Name = "CardId", DbType = "INTEGER")]
        public int CardId { get; set; }

        [Column(Name = "Name", DbType = "VARCHAR")]
        public string Name { get; set; }

        [Column(Name = "Description", DbType = "VARCHAR")]
        public string Description { get; set; }
    }

    public partial class Trait
    {
        public static Trait GetTrait(int cardId, TraitNameEnum traitName, TraitDescEnum traitDesc)
        {
            return new Trait()
            {
                CardId = cardId,
                Name = traitName.ToString(),
                Description = traitDesc.ToString()
            };
        }

    }
}