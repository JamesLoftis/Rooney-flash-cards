﻿namespace RooneyFoundationCardApp
{
    public enum TraitDescEnum
    {
        Basic,
        Intermediate,
        Advanced,
        a,
        e,
        i,
        o,
        u,
        y,
        ar,
        er,
        ir,
        or,
        ur,
        ng,
        nk,
        ind,
        old,
        ild,
        ost,
        olt,
        oll
    }
}