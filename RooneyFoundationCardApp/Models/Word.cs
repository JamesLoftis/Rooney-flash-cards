﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;
using Microsoft.Xna.Framework.Graphics;
using RooneyFoundationCardApp.Data;
using RooneyFoundationCardApp.Extensions;

namespace RooneyFoundationCardApp
{
    [Table(Name = "Words")]
    public partial class Word
    {
        public Word(string name, int card0, int card1, int card2)
        {
            Name = name;
            Card0 = card0;
            Card1 = card1;
            Card2 = card2;
        }
            
        [Column(Name = "Id", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int Id { get; set; }

        [Column(Name = "Name", DbType = "VARCHAR")]
        public string Name { get; set; }

        [Column(Name = "Card0", DbType = "INTEGER")]
        public int Card0 { get; set; }

        [Column(Name = "Card1", DbType = "INTEGER")]
        public int Card1 { get; set; }

        [Column(Name = "Card1", DbType = "INTEGER")]
        public int Card2 { get; set; }
    }
    public partial class Word
    {
        public int[] Cards
        {
            get
            {
                var array = new int[3];
                array[0] = Card0;
                array[1] = Card1;
                array[2] = Card2;
                return array;
            }
        }

        public static Word NewWord(string name, string card0, string card1, string card2, IDatabaseContext context) 
        {
            return new Word(name, context.GetCardId(card0), context.GetCardId(card1), context.GetCardId(card2));
        }
    }
}
